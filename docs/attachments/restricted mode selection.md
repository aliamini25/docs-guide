---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements

# Embedded files
24779fac28c9d56da513faac584bd03557beb8ab: [[Pasted image 20221025193436.png]]

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://excalidraw.com",
	"elements": [
		{
			"type": "image",
			"version": 199,
			"versionNonce": 296705483,
			"isDeleted": false,
			"id": "DLiEA72C",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -467.49576851861065,
			"y": -34.62257591506855,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 664.4939691527105,
			"height": 365.164453325664,
			"seed": 55918,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1666714243905,
			"link": null,
			"locked": false,
			"status": "pending",
			"fileId": "24779fac28c9d56da513faac584bd03557beb8ab",
			"scale": [
				1,
				1
			]
		},
		{
			"type": "ellipse",
			"version": 211,
			"versionNonce": 701921381,
			"isDeleted": false,
			"id": "XTYz8UCAFIl91KHQfhsmf",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "dashed",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -352.3341372742742,
			"y": 234.84125039832085,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 270.1008630698915,
			"height": 67.94983133605832,
			"seed": 114308107,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1666714243905,
			"link": null,
			"locked": false
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#c92a2a",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "dashed",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStrokeSharpness": "sharp",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"currentItemLinearStrokeSharpness": "round",
		"gridSize": null,
		"colorPalette": {}
	},
	"files": {}
}
```
%%